package decorator

import "fmt"

type Review interface {
	Send(orderID, rating int, comment string)
}

type OrderReview struct{}

func (r *OrderReview) Send(orderID, rating int, comment string) {
	fmt.Println(fmt.Sprintf("User send review for order = %d with rating = %d and with comment = %s", orderID, rating, comment))
}

type DeliveryOrderReview struct {
	review Review
}

func (r *DeliveryOrderReview) Send(orderID, rating int, comment string) {
	deliveryOrderID := orderID * 2

	r.review.Send(deliveryOrderID, rating, comment)
}

type TakeawayOrderReview struct {
	review Review
}

func (r *TakeawayOrderReview) Send(orderID, rating int, comment string) {
	takeawayOrderID := orderID * 4

	r.review.Send(takeawayOrderID, rating, comment)
}
